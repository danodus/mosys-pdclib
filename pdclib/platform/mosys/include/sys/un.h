#ifndef _INCLUDE_SYS_UN_H
#define _INCLUDE_SYS_UN_H

#ifndef _SA_FAMILY_T
#define _SA_FAMILY_T
typedef unsigned short sa_family_t;
#endif

struct sockaddr_un {
    sa_family_t sun_family;
    char sun_path[108];
};

#endif
