#ifndef _INCLUDE_SYS_SOCKET_H
#define _INCLUDE_SYS_SOCKET_H

#define SOCK_RAW 1
#define SOCK_DGRAM 2
#define SOCK_SEQPACKET 4
#define SOCK_STREAM 3

#define AF_INET 1
#define AF_INET6 2
#define AF_UNIX 3
#define AF_LOCAL 3
#define AF_UNSPEC 0

#ifndef _SA_FAMILY_T
#define _SA_FAMILY_T
typedef unsigned short sa_family_t;
#endif

#ifndef _SOCKLEN_T
#define _SOCKLEN_T
typedef unsigned long socklen_t;
#endif

struct sockaddr {
    sa_family_t sa_family;
    char        sa_data[14];
};

int accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
int connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
ssize_t recv(int sockfd, void *buf, size_t len, int flags);
ssize_t recvfrom(int sockfd, void *buf, size_t len, int flags, struct sockaddr *src_addr, socklen_t *addrlen);
ssize_t send(int sockfd, const void *buf, size_t len, int flags);
ssize_t sendto(int sockfd, const void *buf, size_t len, int flags, const struct sockaddr *dest_addr, socklen_t addrlen);
int socket(int domain, int type, int protocol);
int socketpair(int domain, int type, int protocol, int sv[2]);

#endif
