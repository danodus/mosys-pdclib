#ifndef _INCLUDE_SYS_IOCTL_H
#define _INCLUDE_SYS_IOCTL_H

#include <unistd.h>
#include <sys/syscall.h>

#include <stdarg.h>

int ioctl(int fd, unsigned long request, ...);

#endif
