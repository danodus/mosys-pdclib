#ifndef _INCLUDE_ASM_IOCTLS_H
#define _INCLUDE_ASM_IOCTLS_H

#include <asm/ioctl.h>

// This uses the same macros as Linux with the same values, for compatibility

#define TCGETS          _IO(    'T', 0x01)
#define TCSETS          _IO(    'T', 0x02)
#define TCSETSW         _IO(    'T', 0x03)
#define TCSETSF         _IO(    'T', 0x04)
#define TCGETA          _IO(    'T', 0x05)
#define TCSETA          _IO(    'T', 0x06)
#define TCSETAW         _IO(    'T', 0x07)
#define TCSETAF         _IO(    'T', 0x08)
#define TCSBRK          _IO(    'T', 0x09)
#define TCXONC          _IO(    'T', 0x0A)
#define TCFLSH          _IO(    'T', 0x0B)
#define TIOCEXCL        _IO(    'T', 0x0C)
#define TIOCNXCL        _IO(    'T', 0x0D)
#define TIOCSCTTY       _IO(    'T', 0x0E)
#define TIOCGPGRP       _IO(    'T', 0x0F)
#define TIOCSPGRP       _IO(    'T', 0x10)
#define TIOCOUTQ        _IO(    'T', 0x11)
#define TIOCSTI         _IO(    'T', 0x12)
#define TIOCGWINSZ      _IO(    'T', 0x13)
#define TIOCSWINSZ      _IO(    'T', 0x14)
#define TIOCMGET        _IO(    'T', 0x15)
#define TIOCMBIS        _IO(    'T', 0x16)
#define TIOCMBIC        _IO(    'T', 0x17)
#define TIOCMSET        _IO(    'T', 0x18)
#define TIOCGSOFTCAR    _IO(    'T', 0x19)
#define TIOCSSOFTCAR    _IO(    'T', 0x1A)
#define FIONREAD        _IO(    'T', 0x1B)
#define TIOCINQ         FIONREAD
#define TIOCLINUX       _IO(    'T', 0x1C)
#define TIOCCONS        _IO(    'T', 0x1D)
#define TIOCGSERIAL     _IO(    'T', 0x1E)
#define TIOCSSERIAL     _IO(    'T', 0x1F)
#define TIOCPKT         _IO(    'T', 0x20)
#define FIONBIO         _IO(    'T', 0x21)
#define TIOCNOTTY       _IO(    'T', 0x22)
#define TIOCSETD        _IO(    'T', 0x23)
#define TIOCGETD        _IO(    'T', 0x24)

#define TCSBRKP         _IO(    'T', 0x25)
#define TIOCSBRK        _IO(    'T', 0x27)
#define TIOCCBRK        _IO(    'T', 0x28)
#define TIOCGSID        _IO(    'T', 0x29)
#define TCGETS2         _IOR(   'T', 0x2A, struct termios2)
#define TCSETS2         _IOW(   'T', 0x2B, struct termios2)
#define TCSETSW2        _IOW(   'T', 0x2C, struct termios2)
#define TCSETSF2        _IOW(   'T', 0x2D, struct termios2)
#define TIOCGRS485      _IO(    'T', 0x2E)
#ifndef TIOCSRS485
#define TIOCSRS485      _IO(    'T', 0x2F)
#endif
#define TIOCGPTN        _IOR(   'T', 0x30, unsigned int)
#define TIOCSPTLCK      _IOW(   'T', 0x31, int)
#define TIOCGDEV        _IOR(   'T', 0x32, unsigned int)
#define TCGETX          _IO(    'T', 0x32)
#define TCSETX          _IO(    'T', 0x33)
#define TCSETXF         _IO(    'T', 0x34)
#define TCSETXW         _IO(    'T', 0x35)
#define TIOCSIG         _IOW(   'T', 0x36, int)
#define TIOCVHANGUP     _IO(    'T', 0x37)
#define TIOCGPKT        _IOR(   'T', 0x38, int)
#define TIOCGPTLCK      _IOR(   'T', 0x39, int)
#define TIOCGEXCL       _IOR(   'T', 0x40, int)
#define TIOCGPTPEER     _IO(    'T', 0x41)
#define TIOCGISO7816    _IOR(   'T', 0x42, struct serial_iso7816)
#define TIOCSISO7816    _IOWR(  'T', 0x43, struct serial_iso7816)

#define FIONCLEX        _IO(    'T', 0x50)
#define FIOCLEX         _IO(    'T', 0x51)
#define FIOASYNC        _IO(    'T', 0x52)
#define TIOCSERCONFIG   _IO(    'T', 0x53)
#define TIOCSERGWILD    _IO(    'T', 0x54)
#define TIOCSERSWILD    _IO(    'T', 0x55)
#define TIOCGLCKTRMIOS  _IO(    'T', 0x56)
#define TIOCSLCKTRMIOS  _IO(    'T', 0x57)
#define TIOCSERGSTRUCT  _IO(    'T', 0x58)
#define TIOCSERGETLSR   _IO(    'T', 0x59)
#define TIOCSERGETMULTI _IO(    'T', 0x5A)
#define TIOCSERSETMULTI _IO(    'T', 0x5B)

#define TIOCMIWAIT      _IO(    'T', 0x5C)
#define TIOCGICOUNT     _IO(    'T', 0x5D)

#ifndef FIOQSIZE
#define FIOQSIZE        _IO(    'T', 0x60)
#endif

#define TIOCPKT_DATA         0
#define TIOCPKT_FLUSHREAD    1
#define TIOCPKT_FLUSHWRITE   2
#define TIOCPKT_STOP         4
#define TIOCPKT_START        8
#define TIOCPKT_NOSTOP      16
#define TIOCPKT_DOSTOP      32
#define TIOCPKT_IOCTL       64

#define TIOCSER_TEMT    0x01

#endif
