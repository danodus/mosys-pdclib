#ifndef _INCLUDE_ERR_H
#define _INCLUDE_ERR_H

#include <stdnoreturn.h>

noreturn void err(int eval, const char *fmt, ...);
noreturn void errx(int eval, const char *fmt, ...);
void warn(const char *fmt, ...);
void warnx(const char *fmt, ...);

#include <stdarg.h>

noreturn void verr(int eval, const char *fmt, va_list args);
noreturn void verrx(int eval, const char *fmt, va_list args);
void vwarn(const char *fmt, va_list args);
void vwarnx(const char *fmt, va_list args);

#endif
