#ifndef _INCLUDE_DIRENT_H
#define _INCLUDE_DIRENT_H

#ifndef _INO_T
typedef unsigned long ino_t;
#endif

struct dirent {
    ino_t  d_ino;
    char   d_name[33];
};

#endif
