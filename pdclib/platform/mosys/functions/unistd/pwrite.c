#include <unistd.h>

#include <sys/syscall.h>

ssize_t pwrite(int fildes, const void *buf, size_t nbyte, off_t offset) {
    return syscall(SYS_pwrite, fildes, buf, nbyte, offset);
}
