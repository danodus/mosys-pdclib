#include <unistd.h>

#include <sys/syscall.h>

int fchdir(int fd) {
    return syscall(SYS_fchdir, fd);
}
