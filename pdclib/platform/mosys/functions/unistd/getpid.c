#include <sys/types.h>
#include <unistd.h>

#include <sys/syscall.h>

pid_t getpid(void) {
    return syscall(SYS_getpid);
}

pid_t getppid(void) {
    return syscall(SYS_getppid);
}
