#include <sys/types.h>
#include <sys/socket.h>

#include <stddef.h>
#include <sys/syscall.h>

ssize_t sendto(int sockfd, const void *buf, size_t len, int flags,
               const struct sockaddr *dest_addr, socklen_t addrlen);

ssize_t send(int sockfd, const void *buf, size_t len, int flags) {
    return sendto(sockfd, buf, len, flags, NULL, 0);
}

ssize_t sendto(int sockfd, const void *buf, size_t len, int flags,
               const struct sockaddr *dest_addr, socklen_t addrlen) {
    return syscall(SYS_sendto, sockfd, buf, len, flags, dest_addr, addrlen);
}
