#include <sys/types.h>
#include <sys/socket.h>

#include <stddef.h>
#include <sys/syscall.h>

ssize_t recvfrom(int sockfd, void *buf, size_t len, int flags,
                 struct sockaddr *src_addr, socklen_t *addrlen);

ssize_t recv(int sockfd, void *buf, size_t len, int flags) {
    return recvfrom(sockfd, buf, len, flags, NULL, NULL);
}

ssize_t recvfrom(int sockfd, void *buf, size_t len, int flags,
                 struct sockaddr *src_addr, socklen_t *addrlen) {
    return syscall(SYS_recvfrom, sockfd, buf, len, flags, src_addr, addrlen);
}
