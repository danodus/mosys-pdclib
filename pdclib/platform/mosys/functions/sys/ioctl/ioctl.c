#include <sys/ioctl.h>

int ioctl(int fd, unsigned long request, ...) {
    va_list ap;
    va_start(ap, request);
    unsigned long argp = va_arg(ap, unsigned long);
    int result = syscall(SYS_ioctl, fd, request, argp);
    va_end(ap);
    return result;
}
