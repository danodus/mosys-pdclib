    .text

    .globl  _start
    .type   _start, @function
_start:
    | Push envp, argv, argc
    move.l  %a1,-(%sp)
    move.l  %a0,-(%sp)
    move.l  %d0,-(%sp)

    | Load GOT register
    movea.l #_GLOBAL_OFFSET_TABLE_,%a5

    | Store envp to environ
    movea.l environ@GOT(%a5),%a2
    move.l  %a1,(%a2)

    | Store argv[0] to __progname
    movea.l __progname@GOT(%a5),%a2
    move.l  (0,%a0),(%a2)

    | Call main()
    move.l  main@GOT(%a5),%d0
    movea.l %d0,%a0
    jsr     (%a0)
    add.l   #12,%sp

    | Push main return value and call exit()
    move.l  %d0,-(%sp)
    move.l  exit@GOT(%a5),%d0
    movea.l %d0,%a0
    jsr     (%a0)
    add.l   #4,%sp
